package models;

public abstract class Personne {
	
	private int idPersonne;	
	private Renseignement renseignement;
	private String nom;
	private String prenom;
	
	/**
	 * @param idPersonne
	 * @param renseignement
	 * @param nom
	 * @param prenom
	 */
	public Personne(int idPersonne, 
			Renseignement renseignement, 
			String nom, 
			String prenom) {
		this.setId(idPersonne);
		this.setRenseignement(renseignement);
		this.setNom(nom);
		this.setPrenom(prenom);
	}

	/**
	 * @return the idPersonne
	 */
	public int getId() {
		return idPersonne;
	}
	
	/**
	 * @param idPersonne
	 */
	public void setId(int idPersonne) {
		this.idPersonne = idPersonne;
	}

	/**
	 * @return the renseignement
	 */
	public Renseignement getRenseignement() {
		return renseignement;
	}

	/**
	 * @param renseignement
	 */
	public void setRenseignement(Renseignement renseignement) {
		this.renseignement = renseignement;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Personne [idPersonne=" + idPersonne 
				+ ", renseignement=" + renseignement 
				+ ", nom=" + nom
				+ ", prenom=" + prenom + "]";
	}
	
}
