package models;

public class Specialite {
	
	private int idSpecialite;
	private String nomSpecialite;
	
	/**
	 * @param idSpecialite
	 * @param nomSpecialite
	 */
	public Specialite(int idSpecialite, String nomSpecialite) {
		this.setIdSpecialite(idSpecialite);
		this.setNomSpecialite(nomSpecialite);
	}

	/**
	 * @return the idSpecialite
	 */
	public int getIdSpecialite() {
		return idSpecialite;
	}

	/**
	 * @param idSpecialite the idSpecialite to set
	 */
	public void setIdSpecialite(int idSpecialite) {
		this.idSpecialite = idSpecialite;
	}

	/**
	 * @return the nomSpecialite
	 */
	public String getNomSpecialite() {
		return nomSpecialite;
	}

	/**
	 * @param nomSpecialite the nomSpecialite to set
	 */
	public void setNomSpecialite(String nomSpecialite) {
		this.nomSpecialite = nomSpecialite;
	}

	@Override
	public String toString() {
		return "[" + this.getNomSpecialite() + "]";
	}
	
}
