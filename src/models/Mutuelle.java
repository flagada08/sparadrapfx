package models;

public class Mutuelle {
	
	private int idMutuelle, idRenseignement;
	private String nomMutuelle, departementMutuelle;
	private int tauxRemboursement;
	
	/**
	 * @param idMutuelle
	 * @param idRenseignement
	 * @param nomMutuelle
	 * @param departementMutuelle
	 * @param tauxRemboursement
	 */
	public Mutuelle(int idMutuelle, 
			int idRenseignement, 
			String nomMutuelle, 
			String departementMutuelle, 
			int tauxRemboursement) {
		this.setIdMutuelle(idMutuelle);
		this.setIdRenseignement(idRenseignement);
		this.setNomMutuelle(nomMutuelle);
		this.setDepartementMutuelle(departementMutuelle);
		this.setTauxRemboursement(tauxRemboursement);
	}

	/**
	 * @return the idMutuelle
	 */
	public int getIdMutuelle() {
		return idMutuelle;
	}

	/**
	 * @param idMutuelle the idMutuelle to set
	 */
	public void setIdMutuelle(int idMutuelle) {
		this.idMutuelle = idMutuelle;
	}

	/**
	 * @return the idRenseignement
	 */
	public int getIdRenseignement() {
		return idRenseignement;
	}

	/**
	 * @param idRenseignement the idRenseignement to set
	 */
	public void setIdRenseignement(int idRenseignement) {
		this.idRenseignement = idRenseignement;
	}

	/**
	 * @return the nomMutuelle
	 */
	public String getNomMutuelle() {
		return nomMutuelle;
	}

	/**
	 * @param nomMutuelle the nomMutuelle to set
	 */
	public void setNomMutuelle(String nomMutuelle) {
		this.nomMutuelle = nomMutuelle;
	}

	/**
	 * @return the departementMutuelle
	 */
	public String getDepartementMutuelle() {
		return departementMutuelle;
	}

	/**
	 * @param departementMutuelle the departementMutuelle to set
	 */
	public void setDepartementMutuelle(String departementMutuelle) {
		this.departementMutuelle = departementMutuelle;
	}

	/**
	 * @return the tauxRemboursement
	 */
	public int getTauxRemboursement() {
		return tauxRemboursement;
	}

	/**
	 * @param tauxRemboursement the tauxRemboursement to set
	 */
	public void setTauxRemboursement(int tauxRemboursement) {
		this.tauxRemboursement = tauxRemboursement;
	}

	@Override
	public String toString() {
		return "[" + this.getNomMutuelle() + "]";
	}
	
}
