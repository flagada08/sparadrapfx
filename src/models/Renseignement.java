package models;

public class Renseignement {
	
	private int idRenseignement;
	private String adresse;
	private String codePostal;
	private String ville;
	private String numTelephone;
	private String email;
	
	/**
	 * @param idRenseignement
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param numTelephone
	 * @param email
	 */
	public Renseignement(int idRenseignement, 
			String adresse, 
			String codePostal, 
			String ville, 
			String numTelephone, 
			String email) {
		this.setIdRenseignement(idRenseignement);
		this.setAdresse(adresse);
		this.setCodePostal(codePostal);
		this.setVille(ville);
		this.setNumTel(numTelephone);
		this.setEmail(email);
	}

	/**
	 * @return the idRenseignement
	 */
	public int getIdRenseignement() {
		return idRenseignement;
	}

	/**
	 * @param idRenseignement the idRenseignement to set
	 */
	public void setIdRenseignement(int idRenseignement) {
		this.idRenseignement = idRenseignement;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * @param codePostal the codePostal to set
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville the ville to set
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return the numTelephone
	 */
	public String getNumTel() {
		return numTelephone;
	}

	/**
	 * @param numTelephone the numTelephone to set
	 */
	public void setNumTel(String numTelephone) {
		this.numTelephone = numTelephone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "[" + adresse 
				+ ", " + codePostal 
				+ ", " + ville 
				+ ", " + numTelephone 
				+ ", " + email + "]";
	}
	
}
