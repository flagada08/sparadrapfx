package models;

public class CategorieMedicament {

	private int idCategorie;
	private String nomCategorie;
	
	/**
	 * @param idCategorie
	 * @param nomCategorie
	 */
	public CategorieMedicament(int idCategorie, String nomCategorie) {
		this.idCategorie = idCategorie;
		this.nomCategorie = nomCategorie;
	}

	/**
	 * @return the idCategorie
	 */
	public int getIdCategorie() {
		return idCategorie;
	}

	/**
	 * @param idCategorie the idCategorie to set
	 */
	public void setIdCategorie(int idCategorie) {
		this.idCategorie = idCategorie;
	}

	/**
	 * @return the nomCategorie
	 */
	public String getNomCategorie() {
		return nomCategorie;
	}

	/**
	 * @param nomCategorie the nomCategorie to set
	 */
	public void setNomCategorie(String nomCategorie) {
		this.nomCategorie = nomCategorie;
	}

	@Override
	public String toString() {
		return "CategorieMedicament [idCategorie=" + idCategorie 
				+ ", nomCategorie=" + nomCategorie + "]";
	}
	
}
