package models;

import java.sql.Date;

public class Client extends Personne {
	
	private Long numSecuSocial;
	private Medecin medecin;
	private Date dateNaissance;

	/**
	 * @param idPersonne
	 * @param renseignement
	 * @param nom
	 * @param prenom
	 * @param numSecuSocial
	 * @param medecin
	 * @param dateNaissance
	 */
	public Client(int idPersonne, 
			Renseignement renseignement, 
			String nom, 
			String prenom, 
			Long numSecuSocial, 
			Medecin medecin,
			Date dateNaissance) {
		super(idPersonne, renseignement, nom, prenom);
		this.setNumSecuSocial(numSecuSocial);
		this.setMedecin(medecin);
		this.setDateNaissance(dateNaissance);
	}

	/**
	 * @return the numSecuSocial
	 */
	public Long getNumSecuSocial() {
		return numSecuSocial;
	}

	/**
	 * @param numSecuSocial the numSecuSocial to set
	 */
	public void setNumSecuSocial(Long numSecuSocial) {
		this.numSecuSocial = numSecuSocial;
	}

	/**
	 * @return the medecin
	 */
	public Medecin getMedecin() {
		return medecin;
	}

	/**
	 * @param medecin the medecin to set
	 */
	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}

	/**
	 * @return the dateNaissance
	 */
	public Date getDateNaissance() {
		return dateNaissance;
	}

	/**
	 * @param dateNaissance the dateNaissance to set
	 */
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	@Override
	public String toString() {
		return "Client [dateNaissance=" + dateNaissance + "]";
	}

}
