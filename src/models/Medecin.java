package models;

import java.util.ArrayList;
import java.util.List;

public class Medecin extends Personne {
	
	private int numAgrement;
	private int idSpecialite;
	private List<Client> patients = new ArrayList<>();
	private List<Ordonnance> ordonnances = new ArrayList<>();
	
	/**
	 * @param idPersonne
	 * @param renseignement
	 * @param nom
	 * @param prenom
	 * @param numAgrement
	 * @param idSpecialite
	 * @param patients
	 * @param ordonnances
	 */
	public Medecin(int idPersonne, 
			Renseignement renseignement, 
			String nom, 
			String prenom, 
			int numAgrement, 
			int idSpecialite, 
			List<Client> patients, 
			List<Ordonnance> ordonnances) {
		super(idPersonne, renseignement, nom, prenom);
		this.setNumAgrement(numAgrement);
		this.setIdSpecialite(idSpecialite);
		this.setPatients(patients);
		this.setOrdonnances(ordonnances);
	}

	/**
	 * @return the numAgrement
	 */
	public int getNumAgrement() {
		return numAgrement;
	}

	/**
	 * @param numAgrement the numAgrement to set
	 */
	public void setNumAgrement(int numAgrement) {
		this.numAgrement = numAgrement;
	}

	/**
	 * @return the idSpecialite
	 */
	public int getIdSpecialite() {
		return idSpecialite;
	}

	/**
	 * @param idSpecialite the idSpecialite to set
	 */
	public void setIdSpecialite(int idSpecialite) {
		this.idSpecialite = idSpecialite;
	}

	/**
	 * @return the patients
	 */
	public List<Client> getPatients() {
		return patients;
	}

	/**
	 * @param patients the patients to set
	 */
	public void setPatients(List<Client> patients) {
		this.patients = patients;
	}

	/**
	 * @return the ordonnances
	 */
	public List<Ordonnance> getOrdonnances() {
		return ordonnances;
	}

	/**
	 * @param ordonnances the ordonnances to set
	 */
	public void setOrdonnances(List<Ordonnance> ordonnances) {
		this.ordonnances = ordonnances;
	}

	@Override
	public String toString() {
		return "[" + this.getNumAgrement() 
				+ " " + this.getNom()
				+ " " + this.getPrenom() + "]";
	}
	
}
