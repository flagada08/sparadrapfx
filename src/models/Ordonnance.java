package models;

import java.sql.Date;

public class Ordonnance {
	private int idOrdonnance, numAgrement;
	Long numSecuSocial;
	private Date dateOrdonnance;
	
	/**
	 * @param idOrdonnance
	 * @param numAgrement
	 * @param numSecuSocial
	 * @param dateOrdonnance
	 */
	public Ordonnance(int idOrdonnance, 
			int numAgrement, 
			Long numSecuSocial, 
			Date dateOrdonnance) {
		this.setIdOrdonnance(idOrdonnance);
		this.setNumAgrement(numAgrement);
		this.setNumSecuSocial(numSecuSocial);
		this.setDateOrdonnance(dateOrdonnance);
	}

	/**
	 * @return the idOrdonnance
	 */
	public int getIdOrdonnance() {
		return idOrdonnance;
	}

	/**
	 * @param idOrdonnance the idOrdonnance to set
	 */
	public void setIdOrdonnance(int idOrdonnance) {
		this.idOrdonnance = idOrdonnance;
	}

	/**
	 * @return the numAgrement
	 */
	public int getNumAgrement() {
		return numAgrement;
	}

	/**
	 * @param numAgrement the numAgrement to set
	 */
	public void setNumAgrement(int numAgrement) {
		this.numAgrement = numAgrement;
	}

	/**
	 * @return the numSecuSocial
	 */
	public Long getNumSecuSocial() {
		return numSecuSocial;
	}

	/**
	 * @param numSecuSocial the numSecuSocial to set
	 */
	public void setNumSecuSocial(Long numSecuSocial) {
		this.numSecuSocial = numSecuSocial;
	}

	/**
	 * @return the dateOrdonnance
	 */
	public Date getDateOrdonnance() {
		return dateOrdonnance;
	}

	/**
	 * @param dateOrdonnance the dateOrdonnance to set
	 */
	public void setDateOrdonnance(Date dateOrdonnance) {
		this.dateOrdonnance = dateOrdonnance;
	}

	@Override
	public String toString() {
		return "[" + this.getIdOrdonnance() 
				+ " " + this.getNumSecuSocial() 
				+ " " + this.getDateOrdonnance() + "]";
	}
	
}
