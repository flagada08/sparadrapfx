package models;

import java.sql.Date;

public class Medicament {

	private int idMedicament;
	private int idCategorie;
	private String nomMedicament;
	private Date dateMedicament;
	private Float prixMedicament;
	private Long stockMedicament;
	private int qteMedicament;
	
	/**
	 * @param idMedicament
	 * @param idCategorie
	 * @param nomMedicament
	 * @param dateMedicament
	 * @param prixMedicament
	 * @param stockMedicament
	 * @parma qteMedicament
	 */
	public Medicament(int idMedicament, 
			int idCategorie, 
			String nomMedicament, 
			Date dateMedicament,
			Float prixMedicament, 
			Long stockMedicament,
			int qteMedicament) {
		this.setIdMedicament(idMedicament);
		this.setIdCategorie(idCategorie);
		this.setNomMedicament(nomMedicament);
		this.setDateMedicament(dateMedicament);
		this.setPrixMedicament(prixMedicament);
		this.setStockMedicament(stockMedicament);
		this.setQteMedicament(qteMedicament);
	}
	
	/**
	 * @return the idMedicament
	 */
	public int getIdMedicament() {
		return idMedicament;
	}
	
	/**
	 * @param idMedicament the idMedicament to set
	 */
	public void setIdMedicament(int idMedicament) {
		this.idMedicament = idMedicament;
	}
	
	/**
	 * @return the idCategorie
	 */
	public int getIdCategorie() {
		return idCategorie;
	}
	
	/**
	 * @param idCategorie the idCategorie to set
	 */
	public void setIdCategorie(int idCategorie) {
		this.idCategorie = idCategorie;
	}
	
	/**
	 * @return the nomMedicament
	 */
	public String getNomMedicament() {
		return nomMedicament;
	}
	
	/**
	 * @param nomMedicament the nomMedicament to set
	 */
	public void setNomMedicament(String nomMedicament) {
		this.nomMedicament = nomMedicament;
	}
	
	/**
	 * @return the dateMedicament
	 */
	public Date getDateMedicament() {
		return dateMedicament;
	}
	
	/**
	 * @param dateMedicament the dateMedicament to set
	 */
	public void setDateMedicament(Date dateMedicament) {
		this.dateMedicament = dateMedicament;
	}
	
	/**
	 * @return the prixMedicament
	 */
	public Float getPrixMedicament() {
		return prixMedicament;
	}
	
	/**
	 * @param prixMedicament the prixMedicament to set
	 */
	public void setPrixMedicament(Float prixMedicament) {
		this.prixMedicament = prixMedicament;
	}
	
	/**
	 * @return the stockMedicament
	 */
	public Long getStockMedicament() {
		return stockMedicament;
	}
	
	/**
	 * @param stockMedicament the stockMedicament to set
	 */
	public void setStockMedicament(Long stockMedicament) {
		this.stockMedicament = stockMedicament;
	}

	/**
	 * @return the qteMedicament
	 */
	public int getQteMedicament() {
		return qteMedicament;
	}

	/**
	 * @param qteMedicament the qteMedicament to set
	 */
	public void setQteMedicament(int qteMedicament) {
		this.qteMedicament = qteMedicament;
	}

	@Override
	public String toString() {
		return "[" + this.getNomMedicament() 
				+ " " + getPrixMedicament() + "€"
				+ " " + getStockMedicament() + "]";
	}
		
}
