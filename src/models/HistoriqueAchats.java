package models;

import java.sql.Date;

public class HistoriqueAchats {
	
	private Long numSecuSocial;
	private String nom;
	private String prenom;
	private String nomMedicament;
	private int medecin;
	private Date dateAchat;
	
	/**
	 * @param numSecuSocial
	 * @param nom
	 * @param prenom
	 * @param nomMedicament
	 * @param nomMedecin
	 * @param dateAchat
	 */
	public HistoriqueAchats(Long numSecuSocial, String nom, String prenom, String nomMedicament, int medecin,
			Date dateAchat) {
		super();
		this.setNumSecuSocial(numSecuSocial);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setNomMedicament(nomMedicament);
		this.setMedecin(medecin);
		this.setDateAchat(dateAchat);
	}

	/**
	 * @return the numSecuSocial
	 */
	public Long getNumSecuSocial() {
		return numSecuSocial;
	}

	/**
	 * @param numSecuSocial the numSecuSocial to set
	 */
	public void setNumSecuSocial(Long numSecuSocial) {
		this.numSecuSocial = numSecuSocial;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the nomMedicament
	 */
	public String getNomMedicament() {
		return nomMedicament;
	}

	/**
	 * @param nomMedicament the nomMedicament to set
	 */
	public void setNomMedicament(String nomMedicament) {
		this.nomMedicament = nomMedicament;
	}

	/**
	 * @return the medecin
	 */
	public int getMedecin() {
		return medecin;
	}

	/**
	 * @param medecin the medecin to set
	 */
	public void setMedecin(int medecin) {
		this.medecin = medecin;
	}

	/**
	 * @return the dateAchat
	 */
	public Date getDateAchat() {
		return dateAchat;
	}

	/**
	 * @param dateAchat the dateAchat to set
	 */
	public void setDateAchat(Date dateAchat) {
		this.dateAchat = dateAchat;
	}

	@Override
	public String toString() {
		return "HistoriqueAchats [numSecuSocial=" + numSecuSocial + ", nom=" + nom + ", prenom=" + prenom
				+ ", nomMedicament=" + nomMedicament + ", medecin=" + medecin + ", dateAchat=" + dateAchat + "]";
	}	
	
}
