package models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Achat {
	
	private int idAchat;
	private Client client;
	private Medecin medecin;
	private int idMedicament;
	private int qteMedicament;
	private Date dateAchat;
	private List<Medicament> medicaments = new ArrayList<>();

	/**
	 * @param idAchat
	 * @param client
	 * @param medecin
	 * @param idMedicament
	 * @param qteMedicament
	 * @param dateAchat
	 * @param medicaments
	 */
	public Achat(int idAchat, 
			Client client, 
			Medecin medecin, 
			int idMedicament,
			int qteMedicament,
			Date dateAchat,
			List<Medicament> medicaments) {
		this.setIdAchat(idAchat);
		this.setClient(client);
		this.setMedecin(medecin);
		this.setIdMedicament(idMedicament);
		this.setQteMedicament(qteMedicament);
		this.setDateAchat(dateAchat);
		this.setMedicaments(medicaments);
	}

	/**
	 * @return the idAchat
	 */
	public int getIdAchat() {
		return idAchat;
	}

	/**
	 * @param idAchat the idAchat to set
	 */
	public void setIdAchat(int idAchat) {
		this.idAchat = idAchat;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * @return the medecin
	 */
	public Medecin getMedecin() {
		return medecin;
	}

	/**
	 * @param medecin the medecin to set
	 */
	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}

	/**
	 * @return the idMedicament
	 */
	public int getIdMedicament() {
		return idMedicament;
	}

	/**
	 * @param idMedicament the idMedicament to set
	 */
	public void setIdMedicament(int idMedicament) {
		this.idMedicament = idMedicament;
	}

	/**
	 * @return the qteMedicament
	 */
	public int getQteMedicament() {
		return qteMedicament;
	}

	/**
	 * @param qteMedicament the qteMedicament to set
	 */
	public void setQteMedicament(int qteMedicament) {
		this.qteMedicament = qteMedicament;
	}

	/**
	 * @return the dateAchat
	 */
	public Date getDateAchat() {
		return dateAchat;
	}

	/**
	 * @param dateAchat the dateAchat to set
	 */
	public void setDateAchat(Date dateAchat) {
		this.dateAchat = dateAchat;
	}

	public List<Medicament> getMedicaments() {
		return medicaments;
	}

	public void setMedicaments(List<Medicament> medicaments) {
		this.medicaments = medicaments;
	}

	@Override
	public String toString() {
		return "Achat [idAchat=" + idAchat 
				+ ", client=" + client 
				+ ", medecin=" + medecin
				+ ", dateAchat=" + dateAchat 
				+ ", medicaments=" + medicaments + "]";
	}
	
}
