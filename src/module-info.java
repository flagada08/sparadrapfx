module SparadrapFX {
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.base;
	
	requires java.sql;
	
	requires de.jensd.fx.glyphs.fontawesome;
	
	opens application to javafx.graphics, javafx.fxml;
	opens controllers to javafx.graphics, javafx.fxml;
	opens models to javafx.base;
}
