package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import models.Achat;
import models.HistoriqueAchats;
import models.Medicament;
import models.Personne;

public class AchatDAOImpl implements AchatDAO {
	//CRUD -READ- get Achat
	@Override
	public Achat get(int id) throws SQLException {
		Connection con = Database.getConnection();
		
		Achat achat = null;
		
		String sql = "SELECT "
					+ "* "
					+ "FROM achat "
					+ "WHERE "
					+ "IDACHAT = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			int rsGetIdAchat = rs.getInt("IDACHAT");
			int rsGetIdPersonne = rs.getInt("IDPERSONNE");
			int rsGetNumMedecin = rs.getInt("NUMAMEDECIN");
			int rsGetIdMedicament = rs.getInt("IDMEDICAMENT");
			int rsGetQteMedicament = rs.getInt("QTEMEDICAMENT");
			Date rsGetDateAchat = rs.getDate("DATEACHAT");
			
			achat = new Achat(rsGetIdAchat, 
					new ClientDAOImpl().get(rsGetIdPersonne), 
					new MedecinDAOImpl().get(rsGetNumMedecin), 
					rsGetIdMedicament, 
					rsGetQteMedicament, 
					rsGetDateAchat, 
					null);
		}
		
		Database.closeResultSet(rs);
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return achat;
	}
	//CRUD -READALL- getAll Achat
	@Override
	public List<Achat> getAll() throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "SELECT "
					+ "* "
					+ "FROM "
					+ "achat ";
		
		List<Achat> achats = new ArrayList<>();
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			int rsGetIdAchat = rs.getInt("IDACHAT");
			Date rsGetDateAchat = rs.getDate("DATEACHAT");
			
			Achat achat = new Achat(rsGetIdAchat, null, null, 0, 0, rsGetDateAchat, null);
			achats.add(achat);
		}
		
		return achats;
	}
	//CRUD -CREATE-UPDATE- save Achat
	@Override
	public int save(Achat achat) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	//CRUD -CREATE- insert Achat
	@Override
	public int insert(Achat achat) throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "INSERT INTO achat (IDPERSONNE, IDMEDICAMENT, DATEACHAT) "
				+ "VALUES (?, ?, ?)";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, achat.getClient().getId());
		ps.setInt(2, achat.getIdMedicament());
		ps.setDate(3, achat.getDateAchat());
		
		int result = ps.executeUpdate();
		
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return result;
	}
	//CRUD -UPDATE- update Achat
	@Override
	public int update(Achat achat) throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "UPDATE achat SET IDACHAT = ?, "
				+ "IDPERSONNE = ?, "
				+ "DATEACHAT = ? "
				+ "WHERE IDACHAT = ?";
	
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, achat.getIdAchat());
		ps.setDate(2, achat.getDateAchat());
		ps.setInt(3, achat.getIdAchat());
		
		int result = ps.executeUpdate();
		
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return result;
	}
	//CRUD -DELETE- delete Achat
	@Override
	public int delete(Achat achat) throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "DELETE FROM achat WHERE IDACHAT = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, achat.getIdAchat());
		
		int result = ps.executeUpdate();
		
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return result;
	}
	//CRUD -READ- getHistorique Achats
	@Override
	public ObservableList<HistoriqueAchats> getHistorique() throws SQLException {
		Connection con = Database.getConnection();
		
		ObservableList<HistoriqueAchats> historiqueAchats = FXCollections.observableArrayList();
		
		String sql = "SELECT "
					+ "NUMSECUSOCIAL, NOM, PRENOM, NOMMEDICAMENT, NUMAGREMENT, DATEACHAT "
					+ "FROM personne "
					+ "JOIN achat ON personne.IDPERSONNE = achat.IDPERSONNE "
					+ "JOIN medicament ON achat.IDMEDICAMENT = medicament.IDMEDICAMENT";
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			Long rsGetNumSecuSocial = rs.getLong("NUMSECUSOCIAL");
			String rsGetNom = rs.getString("NOM");
			String rsGetPrenom = rs.getString("PRENOM");
			String rsGetNomMedicament = rs.getString("NOMMEDICAMENT");
			int rsGetMedecin = rs.getInt("NUMAGREMENT");
			Date rsGetDateAchat = rs.getDate("DATEACHAT");
			
			HistoriqueAchats achats = new HistoriqueAchats(rsGetNumSecuSocial, 
					rsGetNom, 
					rsGetPrenom, 
					rsGetNomMedicament, 
					rsGetMedecin, 
					rsGetDateAchat);
			historiqueAchats.add(achats);
		}
		
		Database.closeResultSet(rs);
		Database.closeConnection(con);
		
		return historiqueAchats;
	}
	//CRUD -READ- getLastId Achat
	@Override
	public int getLastId() throws SQLException {
		int id = 0;
		Connection con = Database.getConnection();
				
		String sql = "SELECT MAX(IDACHAT) FROM achat";
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			id = rs.getInt(1);
		}
		
		Database.closeStatement(stm);
		Database.closeConnection(con);
		
		return id;
	}
	
}
