package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Medicament;

public class MedicamentDAOImpl implements MedicamentDAO {

	@Override
	public Medicament get(int id) throws SQLException {
		Connection con = Database.getConnection();
		
		Medicament medicament = null;
		
		String sql = "SELECT "
					+ "* "
					+ "FROM medicament "
					+ "WHERE IDMEDICAMENT = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			int rsGetIdMedicament = rs.getInt(1);
			int rsGetIdCategorie = rs.getInt(2);
			String rsGetIdNomMedicament = rs.getString(3);
			Date rsGetIdDateMedicament = rs.getDate(4);
			Float rsGetIdPrixMedicament = rs.getFloat(5);
			Long rsGetIdStockMedicament = rs.getLong(6);
			
			medicament = new Medicament(rsGetIdMedicament, 
					rsGetIdCategorie, 
					rsGetIdNomMedicament, 
					rsGetIdDateMedicament, 
					rsGetIdPrixMedicament, 
					rsGetIdStockMedicament, 
					0);
		}
		
		Database.closeResultSet(rs);
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return medicament;
	}

	@Override
	public List<Medicament> getAll() throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "SELECT "
					+ "* "
					+ "FROM medicament";
		
		List<Medicament> medicaments = new ArrayList<>();
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			int rsGetIdMedicament = rs.getInt("IDMEDICAMENT");
			int rsGetIdCategorieMedicament = rs.getInt("IDCATEGORIE");
			String rsGetNomMedicament = rs.getString("NOMMEDICAMENT");
			Date rsGetDateMedicament = rs.getDate("DATEMEDICAMENT");
			float rsGetPrixMedicament = rs.getFloat("PRIXMEDICAMENT");
			Long rsGetStockMedicament = rs.getLong("STOCKMEDICAMENT");
			
			
			Medicament medicament = new Medicament(rsGetIdMedicament, 
					rsGetIdCategorieMedicament, 
					rsGetNomMedicament, 
					rsGetDateMedicament, 
					rsGetPrixMedicament, 
					rsGetStockMedicament, 
					0);
			medicaments.add(medicament);
		}
		
		return medicaments;
	}

	@Override
	public int save(Medicament t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Medicament t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Medicament t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Medicament t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
