package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Ordonnance;

public class OrdonnanceDAOImpl implements OrdonnanceDAO {

	@Override
	public Ordonnance get(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ordonnance> getAll() throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "SELECT "
					+ "* "
					+ "FROM ordonnance";
		
		List<Ordonnance> ordonnances = new ArrayList<>();
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			int rsGetIdOrdonnance = rs.getInt("IDORDONNANCE");
			int rsGetNumAgrement = rs.getInt("NUMAGREMENT");
			Long rsGetNumSecuSocial = rs.getLong("NUMSECUSOCIAL");
			Date rsGetDateOrdonnance = rs.getDate("DATEORDONNANCE");
			
			Ordonnance ordonnance = new Ordonnance(rsGetIdOrdonnance, 
					rsGetNumAgrement, 
					rsGetNumSecuSocial, 
					rsGetDateOrdonnance);
			ordonnances.add(ordonnance);
		}
		
		return ordonnances;
	}

	@Override
	public int save(Ordonnance t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Ordonnance t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Ordonnance t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Ordonnance t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

}
