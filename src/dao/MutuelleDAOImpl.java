package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Mutuelle;

public class MutuelleDAOImpl implements MutuelleDAO {

	@Override
	public Mutuelle get(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Mutuelle> getAll() throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "SELECT "
					+ "mutuelle.NOMMUTUELLE "
					+ "FROM mutuelle";
		
		List<Mutuelle> mutuelles = new ArrayList<>();
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			String rsGetNomMutuelle = rs.getString("NOMMUTUELLE");
			
			Mutuelle medecin = new Mutuelle(0, 0, rsGetNomMutuelle, null, 0);
			mutuelles.add(medecin);
		}
		
		return mutuelles;
	}

	@Override
	public int save(Mutuelle t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Mutuelle t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Mutuelle t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Mutuelle t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
