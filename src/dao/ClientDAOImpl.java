package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Client;

public class ClientDAOImpl implements ClientDAO {	
	//CRUD -READ- get Client
	@Override
	public Client get(int id) throws SQLException {
		Connection con = Database.getConnection();
		
		Client client = null;
		
		String sql = "SELECT "
					+ "* "
					+ "FROM "
					+ "personne "
					+ "WHERE "
					+ "IDPERSONNE = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			int rsGetIdClient = rs.getInt("IDPERSONNE");
			String rsGetNomClient = rs.getString("NOM");
			String rsGetPrenomClient = rs.getString("PRENOM");
			Date rsGetDateNaissance = rs.getDate("DATENAISSANCE");
			
			client = new Client(rsGetIdClient, 
					null, 
					rsGetNomClient, 
					rsGetPrenomClient, 
					null, 
					null, 
					rsGetDateNaissance);
		}
		
		Database.closeResultSet(rs);
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return client;
	}
	//CRUD -READALL- getAll Client
	@Override
	public List<Client> getAll() throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "SELECT "
					+ "IDPERSONNE, "
					+ "NOM, "
					+ "PRENOM, "
					+ "DATENAISSANCE "
					+ "FROM "
					+ "personne ";
		
		List<Client> personnes = new ArrayList<>();
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			int rsGetIdClient = rs.getInt("IDPERSONNE");
			String rsGetNomClient = rs.getString("NOM");
			String rsGetPrenomClient = rs.getString("PRENOM");
			Date rsGetDateNaissance = rs.getDate("DATENAISSANCE");
			
			Client client = new Client(rsGetIdClient, 
					null, 
					rsGetNomClient, 
					rsGetPrenomClient, 
					null, null, rsGetDateNaissance);
			personnes.add(client);
		}
		
		return personnes;
	}
	//CRUD -CREATE-UPDATE- save Client
	@Override
	public int save(Client client) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
	//CRUD -CREATE- insert Client
	@Override
	public int insert(Client client) throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "INSERT INTO personne (NOM, PRENOM, DATENAISSANCE) VALUES (?, ?, ?)";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setString(1, client.getNom());
		ps.setString(2, client.getPrenom());
		ps.setDate(3, client.getDateNaissance());
		
		int result = ps.executeUpdate();
		
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return result;
	}
	//CRUD -UPDATE- update Client
	@Override
	public int update(Client client) throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "UPDATE client SET NOM = ?, PRENOM = ?, DATENAISSANCE = ? WHERE ID = ?";
	
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setString(1, client.getNom());
		ps.setString(2, client.getPrenom());
		ps.setDate(3, client.getDateNaissance());
		ps.setInt(4, client.getId());
		
		int result = ps.executeUpdate();
		
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return result;
	}
	//CRUD -DELETE- delete Client
	@Override
	public int delete(Client client) throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "DELETE FROM personne WHERE ID = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, client.getId());
		
		int result = ps.executeUpdate();
		
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return result;
	}
	@Override
	public int getLastId() throws SQLException {
		int id = 0;
		Connection con = Database.getConnection();
				
		String sql = "SELECT MAX(IDPERSONNE) FROM personne";
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			id = rs.getInt(1);
		}
		
		Database.closeStatement(stm);
		Database.closeConnection(con);
		
		return id;
	}

}
