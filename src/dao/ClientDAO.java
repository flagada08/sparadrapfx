package dao;

import java.sql.SQLException;

import models.Client;

public interface ClientDAO extends DAO<Client> {
	//Interface pour les futurs méthodes spécifiques à un client
	int getLastId() throws SQLException;
}
