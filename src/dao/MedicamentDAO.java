package dao;

import java.sql.SQLException;

import models.Medicament;

public interface MedicamentDAO extends DAO<Medicament> {
	//Interface pour les méthodes spécifiques à un médicament
}
