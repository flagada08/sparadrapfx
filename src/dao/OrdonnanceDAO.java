package dao;

import models.Ordonnance;

public interface OrdonnanceDAO extends DAO<Ordonnance> {
	//Interface pour les méthodes spécifiques à une ordonnance
}
