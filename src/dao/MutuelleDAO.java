package dao;

import models.Mutuelle;

public interface MutuelleDAO extends DAO<Mutuelle> {
	//Interface pour les méthodes spécifiques à une mutuelle
}
