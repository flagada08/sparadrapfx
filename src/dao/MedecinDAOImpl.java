package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.Medecin;

public class MedecinDAOImpl implements MedecinDAO {

	@Override
	public Medecin get(int id) throws SQLException {
		Connection con = Database.getConnection();
		
		Medecin medecin = null;
		
		String sql = "SELECT "
					+ "personne.IDPERSONNE, "
					+ "personne.NOM, "
					+ "personne.PRENOM, "
					+ "medecin.NUMAGREMENT, "
					+ "medecin.IDSPECIALITE "
					+ "FROM medecin "
					+ "JOIN personne ON medecin.numagrement = personne.numagrement"
					+ "WHERE NUMAGREMENT = ?";
		
		PreparedStatement ps = con.prepareStatement(sql);
		
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			int rsGetIdPersonne = rs.getInt(1);
			String rsGetNomMedecin = rs.getString(2);
			String rsGetPrenomMedecin = rs.getString(2);
			int rsGetNumAgrement = rs.getInt(4);
			int rsGetIdSpecialite = rs.getInt(5);
			
			medecin = new Medecin(rsGetIdPersonne, 
					null, 
					rsGetNomMedecin, 
					rsGetPrenomMedecin, 
					rsGetNumAgrement, 
					rsGetIdSpecialite, 
					null, 
					null);
		}
		
		Database.closeResultSet(rs);
		Database.closePreparedStatement(ps);
		Database.closeConnection(con);
		
		return medecin;
	}

	@Override
	public List<Medecin> getAll() throws SQLException {
		Connection con = Database.getConnection();
		
		String sql = "SELECT "
					+ "personne.IDPERSONNE, "
					+ "personne.NOM, "
					+ "personne.PRENOM, "
					+ "medecin.NUMAGREMENT, "
					+ "medecin.IDSPECIALITE "
					+ "FROM medecin "
					+ "JOIN personne ON medecin.numagrement = personne.numagrement";
		
		List<Medecin> medecins = new ArrayList<>();
		
		Statement stm = con.createStatement();
		
		ResultSet rs = stm.executeQuery(sql);
		
		while(rs.next()) {
			int rsGetIdPersonne = rs.getInt("IDPERSONNE");
			String rsGetNomMedecin = rs.getString("NOM");
			String rsGetPrenomMedecin = rs.getString("PRENOM");
			int rsGetNumAgrement = rs.getInt("NUMAGREMENT");
			int rsGetIdSpecialite = rs.getInt("IDSPECIALITE");
			
			Medecin medecin = new Medecin(rsGetIdPersonne, 
					null, 
					rsGetNomMedecin, 
					rsGetPrenomMedecin, 
					rsGetNumAgrement, 
					rsGetIdSpecialite, 
					null, 
					null);
			medecins.add(medecin);
		}
		
		return medecins;
	}

	@Override
	public int save(Medecin t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Medecin t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Medecin t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Medecin t) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

}
