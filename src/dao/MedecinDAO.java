package dao;

import models.Medecin;

public interface MedecinDAO extends DAO<Medecin> {
	//Interface pour les méthodes spécifiques à un médecin	
}
