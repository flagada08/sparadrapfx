package dao;

import java.sql.SQLException;

import javafx.collections.ObservableList;
import models.Achat;
import models.HistoriqueAchats;
import models.Medicament;

//Interface pour les méthodes spécifiques à un Achat
public interface AchatDAO extends DAO<Achat> {
	ObservableList<HistoriqueAchats> getHistorique() throws SQLException;
	int getLastId() throws SQLException;
}
