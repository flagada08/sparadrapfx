package application;
	
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import dao.Database;
import dao.ClientDAO;
import dao.ClientDAOImpl;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.HistoriqueAchats;
import models.Personne;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;


public class Main extends Application {
	//Attributs abscisse et ordonnées, destinés aux fonctions 
	//qui rendent la fenêtre movable
	private double xOffset = 0;
	private double yOffset = 0;
    
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/Main.fxml"));
			//MouseEvent pour saisir la fenêtre, sur 
			//pression d'un bouton quelconque de la souris
			root.setOnMousePressed(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
				xOffset = event.getSceneX();
				yOffset = event.getSceneY();
				}
			});
            //MouseEvent pour trainer la fenêtre, lorsque 
            //la pression d'un bouton quelconque de la souris  
			//est maintenue enfoncé
			root.setOnMouseDragged(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
				primaryStage.setX(event.getScreenX() - xOffset);
				primaryStage.setY(event.getScreenY() - yOffset);
				}
			});
			//Nouvelle scene qui prend en paramètre le Main.fxml, 
			//point de départ visuel de l'appli
			Scene scene = new Scene(root);
			//Getter de Stylesheets qui prend comme ressource 
			//le fichier application.css, appliqué à la scene
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			//Flux d'entrée afin de lire correctement l'emplacement du fichier
			InputStream stream = new FileInputStream("src/resources/sparadrap_512.png");
			Image icon = new Image(stream);
			//Application des rendus souhaités pour l'appli :
			//Icone, titre, suppression de l'encadrage fenêtre, etc.
			primaryStage.getIcons().add(icon);
			primaryStage.setTitle("SparadrapFX");
			primaryStage.initStyle(StageStyle.UNDECORATED);
			//Setter de la scène précedemment construite
			primaryStage.setScene(scene);
			//Affichage de l'appli
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws SQLException {
		//Test DB Connection
//		Connection con = Database.getConnection();
//		
//		if(con != null) {
//			System.out.println("DBConnect success");
//		}
		//Test DB getPersonne
//		PersonneDAO personneDAO = new PersonneDAOImpl();
//		Personne personne = personneDAO.get(1);
//		System.out.println(personne);
		//Test DB insertPersonne
//		PersonneDAO personneDAO = new PersonneDAOImpl();
//		Personne personne = new Personne(0, 0, 0, null, 0, 0, "John", "Doe", null);
//		int result = personneDAO.insert(personne);
//		System.out.println(result);
		//Test DB updatePersonne
//		PersonneDAO personneDAO = new PersonneDAOImpl();
//		Personne personne = new Personne(4, 4, 123456789, 123456789654123L, 4, 4, "John", "Doe", null);
//		personneDAO.update(personne);
//		System.out.println(personne);
		//Test DB deletePersonne
//		PersonneDAO personneDAO = new PersonneDAOImpl();		
//		Personne personne = personneDAO.get(4);		
//		System.out.println(personne);
//		int result = personneDAO.delete(personne);
//		System.out.println(result);
		//Test getAll Personnes
//		List<Personne> personnes;
//		PersonneDAO personneDAO = new PersonneDAOImpl();
//		personnes = personneDAO.getAll();
//		for (Personne personne : personnes) {
//			System.out.println(personne);
//		}
		//Test HistoriqueAchats
//		List<HistoriqueAchats> historiques;
//		HistoriqueAchatsDAO histoAchatsDAO = new HistoriqueAchatsDAOImpl();
//		historiques = histoAchatsDAO.getHistorique();
//		for (HistoriqueAchats historique : historiques) {
//			System.out.println(historique);
//		}
		//Démarrage de l'appli
		launch(args);
	}
}
