package controllers;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import dao.AchatDAOImpl;
import dao.ClientDAOImpl;
import dao.MedecinDAOImpl;
import dao.MedicamentDAOImpl;
import dao.MutuelleDAOImpl;
import dao.OrdonnanceDAOImpl;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Achat;
import models.Client;
import models.HistoriqueAchats;
import models.Medecin;
import models.Medicament;
import models.Mutuelle;
import models.Ordonnance;
import models.Specialite;

public class Controller implements Initializable {
	
	@FXML
    private ImageView bgImage;
	
	@FXML
	private Button btnHome;

    @FXML
    private Button btnAchatSansOrdonnance;

    @FXML
    private Button btnValiderSansOrdonnance;
    
    @FXML
    private Button btnAchatAvecOrdonnance;
    
    @FXML
    private Button btnValiderAvecOrdonnance;
    
    @FXML
    private Button btnHistoriqueAchats;

    @FXML
    private ComboBox<Medecin> medecinPatient;

    @FXML
    private ComboBox<Medicament> medicamentClientSansOrdonnance;
    
    @FXML
    private ComboBox<Medicament> medicamentClientAvecOrdonnance;

    @FXML
    private ComboBox<Mutuelle> mutuellePatient;

    @FXML
    private ComboBox<Ordonnance> ordonnancePatient;
    
    @FXML
    private TextField mailClient;
    
    @FXML
    private TextField nomClient;
    
    @FXML
    private TextField nomClientSansOrdonnance;

    @FXML
    private TextField numSecuSocialPatient;

    @FXML
    private TextField prenomClient;
    
    @FXML
    private TextField prenomClientSansOrdonnance;
    
    @FXML
    private TextField telClient;
    
    @FXML
    private TextField dateNaissanceClient;
    
    @FXML
    private TextField dateNaissanceClientSansOrdonnance;
    
    @FXML
    private TextField adresseClient;
    
    @FXML
    private FontAwesomeIconView btnClose;
    
    @FXML
    private Pane pnlStatus;
    
    @FXML
    private Label lblStatusMini;

    @FXML
    private Label lblStatus;
    
    @FXML
    private GridPane pnHome;
    
    @FXML
    private GridPane pnAchatSansOrdonnance;
    
    @FXML
    private GridPane pnAchatAvecOrdonnance;

    @FXML
    private GridPane pnHistoriqueAchats;
    
    @FXML
    private TableView<HistoriqueAchats> tableAchats;
    
    @FXML
    private TableColumn<HistoriqueAchats, Long> colNumSecuSocial;
    
    @FXML
    private TableColumn<HistoriqueAchats, String> colNom;
    
    @FXML
    private TableColumn<HistoriqueAchats, String> colPrenom;
    
    @FXML
    private TableColumn<HistoriqueAchats, String> colMedicament;

    @FXML
    private TableColumn<HistoriqueAchats, String> colMedecin;
    
    @FXML
    private TableColumn<HistoriqueAchats, Date> colDate;
    
    public ObservableList<HistoriqueAchats> achatList = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
		try {
			getDataComboBoxes();
		} catch (Exception e) {
			Alert dialog = new Alert(AlertType.ERROR, "Erreur de récupération des données d'achat", ButtonType.CLOSE);
			Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
			stage.setAlwaysOnTop(true);
			dialog.initStyle(StageStyle.UNDECORATED);
	        dialog.show();
		}
		
		try {
			loadDataHistoriqueAchats();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	private void loadDataHistoriqueAchats() throws SQLException {
		colNumSecuSocial.setCellValueFactory(new PropertyValueFactory<HistoriqueAchats, Long>("numSecuSocial"));
		colNom.setCellValueFactory(new PropertyValueFactory<HistoriqueAchats, String>("nom"));
		colPrenom.setCellValueFactory(new PropertyValueFactory<HistoriqueAchats, String>("prenom"));
		colMedicament.setCellValueFactory(new PropertyValueFactory<HistoriqueAchats, String>("nomMedicament"));
		colMedecin.setCellValueFactory(new PropertyValueFactory<HistoriqueAchats, String>("medecin"));
		colDate.setCellValueFactory(new PropertyValueFactory<HistoriqueAchats, Date>("dateAchat"));
		
		achatList = new AchatDAOImpl().getHistorique();
		tableAchats.setItems(achatList);
	}
	
	private void getDataComboBoxes() throws SQLException {
				
		for (Medecin medecin : new MedecinDAOImpl().getAll()) {
			medecinPatient.getItems().add(medecin);
		}
		
		for (Mutuelle mutuelle : new MutuelleDAOImpl().getAll()) {
			mutuellePatient.getItems().add(mutuelle);
		}
				
		for (Ordonnance ordonnance : new OrdonnanceDAOImpl().getAll()) {
			ordonnancePatient.getItems().add(ordonnance);
		}
		
		for (Medicament medicament : new MedicamentDAOImpl().getAll()) {
			medicamentClientSansOrdonnance.getItems().add(medicament);
		}
		
		for (Medicament medicament : new MedicamentDAOImpl().getAll()) {
			medicamentClientAvecOrdonnance.getItems().add(medicament);
		}
	}

	//Fonction handleClick qui permet le switch des panels d'une scene
    //en poussant ces derniers aux premiers plan
	@FXML
	private void handleClick(ActionEvent event) throws SQLException {
		if(event.getSource() == btnHome) {
			lblStatusMini.setText("/home");
			lblStatus.setText("Home");
			pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(43, 99, 63), CornerRadii.EMPTY, Insets.EMPTY)));
			pnHome.toFront();
		} else if(event.getSource() == btnAchatSansOrdonnance) {
			lblStatusMini.setText("/home/achatSansOrdonnance");
			lblStatus.setText("Achat sans ordonnance");
			pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(99, 63, 43), CornerRadii.EMPTY, Insets.EMPTY)));
			pnAchatSansOrdonnance.toFront();
		} else if(event.getSource() == btnAchatAvecOrdonnance) {
			lblStatusMini.setText("/home/achatAvecOrdonnance");
			lblStatus.setText("Achat avec ordonnance");
			pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(63, 43, 99), CornerRadii.EMPTY, Insets.EMPTY)));
			pnAchatAvecOrdonnance.toFront();
		} else if(event.getSource() == btnHistoriqueAchats) {
			lblStatusMini.setText("/home/achat/historiquesAchats");
			lblStatus.setText("Historique des Achats");
			pnlStatus.setBackground(new Background(new BackgroundFill(Color.rgb(99, 43, 63), CornerRadii.EMPTY, Insets.EMPTY)));
			pnHistoriqueAchats.toFront();
			loadDataHistoriqueAchats();
		}
		
	}
	
	@FXML
	private void handleValidate(MouseEvent event) throws SQLException, ParseException {
		btnValiderSansOrdonnance.getText();
//		int idRenseignement = new RenseignementDAOImpl();
		String nomClient = nomClientSansOrdonnance.getText();
		String prenomClient = prenomClientSansOrdonnance.getText();
		int idMedicamentClient = medicamentClientSansOrdonnance.getValue().getIdMedicament();
		String dateClient = dateNaissanceClientSansOrdonnance.getText();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date parsed = format.parse(dateClient);
		Date sqlDate = new Date(parsed.getTime());
		
		Client clientSansOrdonnance = new Client(0, 
				null, 
				nomClient, 
				prenomClient, 
				null, 
				null, 
				sqlDate);
		
		new ClientDAOImpl().insert(clientSansOrdonnance);
		
		Achat achatSansOrdonnance = new Achat(0, 
				clientSansOrdonnance, 
				null, 
				idMedicamentClient, 
				0, 
				new Date(System.currentTimeMillis()), 
				null);
		
		new AchatDAOImpl().insert(achatSansOrdonnance);
	}
	
	@FXML
	private void handleClose(MouseEvent event) {
		if(event.getSource() == btnClose) {
			System.exit(0);
		}
	}

}
